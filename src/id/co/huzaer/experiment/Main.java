package id.co.huzaer.experiment;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    private static Scanner in = new Scanner(System.in);
    private static List<MataKuliah> matKulList = new ArrayList<>();

    public static void main(String[] args) {
        int menu = 0;
        while (menu != 4) {
            System.out.print("Pendataan dan Perhitungan Kuliah IPS (Indeks Prestasi Semester)" +
                    "\n1. Pendataan Matakuliah\n2. Perhitungan IPS\n3. Update Grade\n4. Keluar\nMasukan Pilihan Anda: ");
            menu = in.nextInt();
            if (menu == 1) {
                pendataanMatkul();
            } else if (menu == 2) {
                perhitunganIPS();
            } else if (menu == 3) {
                updateGrade();
            } else if (menu == 4) {
                System.out.println("---|---|--- Exiting ---|---|---");
            } else {
                System.out.println("Sialahkan pilih menu antara 1-4");
            }
        }
    }

    private static void pendataanMatkul() {
        matKulList.clear();
        System.out.print("\nMasukan jumlah mata kuliah: ");
        int jumlahMatKul = in.nextInt();

        for (int i = 0; i < jumlahMatKul; i++) {
            MataKuliah newMatKul = new MataKuliah();
            System.out.print("Masukan kode mata kuliah: ");
            newMatKul.setMatKulID(in.next());
            System.out.print("Masukan nama mata kuliah: ");
            newMatKul.setMatKulName(in.next());
            String grade;
            int gradeFlag = 0;
            do {
                System.out.print("Masukan grade mata kuliah: ");
                grade = in.next();

                if (grade.equalsIgnoreCase("A") || grade.equalsIgnoreCase("B") || grade.equalsIgnoreCase("C")
                        || grade.equalsIgnoreCase("D") || grade.equalsIgnoreCase("E")) {
                    newMatKul.setGrade(grade);
                    gradeFlag = 1;
                } else {
                    System.out.println("Silahkan masukan grade antara A-E");
                }
            } while (gradeFlag != 1);
            System.out.print("Masukan jumlah sks: ");
            newMatKul.setSks(in.nextInt());
            System.out.println();
            matKulList.add(newMatKul);
        }
    }

    private static void perhitunganIPS() {
        if (matKulList.size() > 0) {
            double gradeSks = 0;
            double sumSks = 0;
            System.out.println("\nMatakuliah yang anda ambil adalah: ");
            for (MataKuliah loop : matKulList) {
                String s = String.format("%s \t%s \t\t%s \t%s", loop.getMatKulID(), loop.getMatKulName(),
                        loop.getGrade(), loop.getSks());
                System.out.println(s);
                gradeSks += (loop.getSks() * getGradeValue(loop.getGrade()));
                sumSks += loop.getSks();
            }
            double ips = gradeSks / sumSks;
            System.out.println(String.format("\nNilai IPS anda adalah %s \n", ips));
        } else {
            System.out.println("\nAnda belum memasukan matakuliah. Silahkan pilih menu nomer 1.");
        }
    }

    private static Integer getGradeValue(String grade) {
        if (grade.equalsIgnoreCase("A")) {
            return 4;
        } else if (grade.equalsIgnoreCase("B")) {
            return 3;
        } else if (grade.equalsIgnoreCase("C")) {
            return 2;
        } else if (grade.equalsIgnoreCase("D")) {
            return 1;
        } else {
            return 0;
        }
    }

    private static void updateGrade() {
        if (matKulList.size() > 0) {
            System.out.print("\nMasukan kode mata kuliah: ");
            String matKulID = in.next();
            System.out.print("Masukan grade baru: ");
            String grade = in.next();

            boolean codeFound = false;
            for (MataKuliah loop : matKulList) {
                if (loop.getMatKulID().equalsIgnoreCase(matKulID)) {
                    codeFound = true;
                    loop.setGrade(grade);
                }
            }

            if (codeFound) {
                for (MataKuliah loop : matKulList) {
                    String s = String.format("%s \t%s \t\t%s \t%s", loop.getMatKulID(), loop.getMatKulName(),
                            loop.getGrade(), loop.getSks());
                    System.out.println(s);
                }
                System.out.println();
            } else {
                System.out.println("\nKode mata kuliah TIDAK DITEMUKAN\n");
            }
        } else {
            System.out.println("\nAnda belum memasukan matakuliah. Silahkan pilih menu nomer 1.");
        }
    }

    private static class MataKuliah {
        private String matKulID;
        private String matKulName;
        private String grade;
        private Integer sks;

        public String getMatKulID() {
            return matKulID;
        }

        public void setMatKulID(String matKulID) {
            this.matKulID = matKulID;
        }

        public String getMatKulName() {
            return matKulName;
        }

        public void setMatKulName(String matKulName) {
            this.matKulName = matKulName;
        }

        public String getGrade() {
            return grade;
        }

        public void setGrade(String grade) {
            this.grade = grade;
        }

        public Integer getSks() {
            return sks;
        }

        public void setSks(Integer sks) {
            this.sks = sks;
        }

        @Override
        public String toString() {
            return matKulID + "\t\t" + matKulName + "\t\t" + grade + "\t\t" + sks;
        }
    }
}
